<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

	<div class="row">
		<div class="col-lg-8">
			<?= $this->session->flashdata('pesan'); ?>
		</div>
	</div>

	<div class="card mb-3 col-lg-10">
		<div class="row no-gutters">
			<div class="col-md-4">
				<img src="<?= base_url('assets/profile/' . $user['foto_profile']) ?>" class="card-img" alt="img user">
			</div>
			<div class="col-md-8">
				<div class="card-body">
					<h4 class="card-title">Nama : <?= $user['nama']; ?></h5>
					<h5 class="card-text">NIK : <?= $user['nik'] ?></p>
					<h5 class="card-text">Username : <?= $user['username'] ?></p>
					<h5 class="card-text">Telp : <?= $user['telp'] ?></p>




					<!-- <p class="card-text"><small class="text-muted">Member since </small></p> -->
					<a class="collapse-item btn btn-success" href="<?= base_url('User/UploadAvatar') ?>">Upload Profile</a>
				</div>
			</div>
		</div>
	</div>

</div>
<!-- /.container-fluid