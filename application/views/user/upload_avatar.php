<!DOCTYPE html>
<html lang="en">

<div class="content">
    <h1>Upload Avatar</h1>

    <form action="UploadAvatar/add" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="foto">Upload Foto</label>
        <div class="custom-file">
          <input type="file" class="custom-file-input" id="foto" name="avatar">
          <label class="custom-file-label" for="foto">Choose file</label>
        </div>
      </div>

        <?php if (isset($error)) : ?>
            <div class="invalid-feedback"><?= $error ?></div>
        <?php endif; ?>

        <div>
            <button type="submit" name="save" class="button button-primary">Upload</button>
        </div>
    </form>