<h1 class="h3 mb-4 mt-5 text-gray-800">Data Pengaduan</h1>

<?= validation_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">', '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  </div>') ?>
<?= $this->session->flashdata('msg'); ?>

<div class="table-responsive">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Isi Laporan</th>
                <th scope="col">Tgl Melapor</th>
                <th scope="col">Foto</th>
                <th scope="col">Status</th>
                <th scope="col">Lihat Detail</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1; ?>
            <?php foreach ($data_pengaduan as $dp) : ?>
                <tr>
                    <th scope="row"><?= $no++; ?></th>
                    <td><?= $dp['nama']; ?></td>
                    <td><?= $dp['isi_laporan']; ?></td>
                    <td><?= $dp['tgl_pengaduan']; ?></td>
                    <td>
                        <img width="100" src="<?= base_url() ?>assets/uploads/<?= $dp['foto']; ?>" alt="">
                    </td>
                    <td>
                        <?php
                        if ($dp['status'] == '0') {

                        ?>
                            <span class="badge badge-secondary">Sedang di verifikasi</span>
                        <?php } elseif ($dp['status'] == 'proses') { ?>
                            <span class="badge badge-primary">Sedang di proses</span>
                        <?php } elseif ($dp['status'] == 'selesai') { ?>
                            <span class="badge badge-success">Selesai di kerjakan</span>
                        <?php } elseif ($dp['status'] == 'tolak') { ?>
                            <span class="badge badge-danger">Pengaduan di tolak</span>
                        <?php }
                        ?>
                    </td>

                    <td class="text-center">
                        <a href="<?= base_url('Masyarakat/DataPengaduanController/pengaduan_detail/' . $dp['id_pengaduan']) ?>" class="btn btn-success"><i class="fas fa-fw fa-eye"></i></a>
                    </td>

                    <?php if ($dp['status'] == '0') : ?>
                        <td>
                            <a href="<?= base_url('Masyarakat/PengaduanController/pengaduan_batal/' . $dp['id_pengaduan']) ?>" class="btn btn-warning">Hapus</a>

                            <a href="<?= base_url('Masyarakat/PengaduanController/edit/' . $dp['id_pengaduan']) ?>" class="btn btn-info">Edit</a>
                        </td>

                    <?php else : ?>
                        <td><small>Tidak ada aksi</small></td>
                    <?php endif; ?>


                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>


</div>