 <div class="container">
   <div class="row justify-content-center">
     <div class="card o-hidden border-0 shadow-lg my-1 col-lg-5">
       <div class="card-body p-0">
         <!-- Nested Row within Card Body -->
         <div class="row justify-content-center">


           <div class="col-lg-12">
             <div class="p-5">
               <div class="text-center">
                 <h1 class="h4 text-gray-900 mb-4">Register</h1>
                 <br>


               </div>

               <?= validation_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">', '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>') ?>

               <?= $this->session->flashdata('msg'); ?>

               <?= form_open('Auth/RegisterController', 'class="user"') ?>
               <div class="col-12">
          <div class="input-group mb-3 from-group">
              <div class="input-group-prepend">  
              </div>
              <input name="nik" type="text" class="input form-control" id="nik" placeholder="NIK" value="<?= set_value('nik') ?>" required="true" aria-label="nik" aria-describedby="basic-addon1" />
              <div class="input-group-append">
              </div>
            </div>
          </div>

               <div class="col-12">
          <div class="input-group mb-3 from-group">
              <div class="input-group-prepend">  
              </div>
              <input name="nama" type="text" class="input form-control" id="nama" placeholder="Nama" value="<?= set_value('nama') ?>" required="true" aria-label="nik" aria-describedby="basic-addon1" />
              <div class="input-group-append">
              </div>
            </div>
          </div>

          
          <div class="col-12">
          <div class="input-group mb-3 from-group">
              <div class="input-group-prepend">  
              </div>
              <input name="username" type="text" class="input form-control" id="username" placeholder="Username" required="true" aria-label="telepon" aria-describedby="basic-addon1" />
              <div class="input-group-append">
              </div>
            </div>
          </div>

               <div class="col-12">
            <div class="input-group mb-3 from-group">
              <div class="input-group-prepend">  
              </div>
              <input name="password" type="password" value="" class="input form-control" id="password" placeholder="password" required="true" aria-label="password" aria-describedby="basic-addon1" />
              <div class="input-group-append">
                <span class="input-group-text" onclick="password_show_hide();">
                  <i class="fas fa-eye" id="show_eye"></i>
                  <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                </span>
              </div>
            </div>
          </div>

               <div class="col-12">
            <div class="input-group mb-3 from-group">
              <div class="input-group-prepend">  
              </div>
              <input name="password2" type="password" value="" class="input form-control" id="password2" placeholder="konfirmasi password" required="true" aria-label="password" aria-describedby="basic-addon1" />
              <div class="input-group-append">
                <span class="input-group-text" onclick="password_show_hide2();">
                  <i class="fas fa-eye" id="show_eye2"></i>
                  <i class="fas fa-eye-slash d-none" id="hide_eye2"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="col-12">
          <div class="input-group mb-3 from-group">
              <div class="input-group-prepend">  
              </div>
              <input name="telp" type="text" class="input form-control" id="telp" placeholder="Telp" required="true" aria-label="telepon" aria-describedby="basic-addon1" />
              <div class="input-group-append">
              </div>
            </div>
          </div>
               
               
               <div class="custom-control custom-checkbox mb-2">
								<input type="checkbox" class="custom-control-input" id="check_confirmation_password" name="confirmation_data" value="agree">
								<label class="custom-control-label" for="check_confirmation_password">Data Tidak Dapat Diubah,
                  <br>Ceklis Bila Anda Sudah Yakin </center></label>
							</div>
               <button type="submit" class="btn btn-primary btn-user btn-block">Register</button>
               </form>
               <hr>
               <div class="text-center">
                 <a class="small" href="<?= base_url('Auth/LoginController') ?>">Sudah punya akun? Login!</a>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>

   </div>

   <script>
     function password_show_hide() {
  var x = document.getElementById("password");
  var show_eye = document.getElementById("show_eye");
  var hide_eye = document.getElementById("hide_eye");
  hide_eye.classList.remove("d-none");
  if (x.type === "password") {
    x.type = "text";
    show_eye.style.display = "none";
    hide_eye.style.display = "block";
  } else {
    x.type = "password";
    show_eye.style.display = "block";
    hide_eye.style.display = "none";
  }
}
   </script>

   <script>
     function password_show_hide2() {
  var x = document.getElementById("password2");
  var show_eye2 = document.getElementById("show_eye2");
  var hide_eye2 = document.getElementById("hide_eye2");
  hide_eye2.classList.remove("d-none");
  if (x.type === "password") {
    x.type = "text";
    show_eye2.style.display = "none";
    hide_eye2.style.display = "block";
  } else {
    x.type = "password";
    show_eye2.style.display = "block";
    hide_eye2.style.display = "none";
  }
}
   </script>