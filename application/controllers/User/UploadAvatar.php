<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UploadAvatar extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Masyarakat_m');
	}

	public function index()
	{
		$data['title'] = 'Profile';

		$masyarakat = $this->db->get_where('masyarakat', ['username' => $this->session->userdata('username')])->row_array();
		$petugas = $this->db->get_where('petugas', ['username' => $this->session->userdata('username')])->row_array();


		if ($masyarakat == TRUE) :
			$data['user'] = $masyarakat;
		elseif ($petugas == TRUE) :
			$data['user'] = $petugas;
		endif;

		$this->load->view('_part2/backend_head', $data);
		$this->load->view('_part2/backend_sidebar_v');
		$this->load->view('_part2/backend_topbar_v');
		$this->load->view('user/upload_avatar');
		$this->load->view('_part2/backend_footer_v');
		$this->load->view('_part2/backend_foot');
	}

	public function add()
	{
		echo "berhasil";
		$config['upload_path']          = './assets/profile';
		$config['allowed_types']        = 'jpg|png|jpeg';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('avatar')) {
			$data['error'] =  $this->upload->display_errors();
		} else {
			$data['error'] =  $this->upload->data();
		}

		$ava['foto_profile'] = $this->upload->data('file_name');
		
		$query=	$this->Masyarakat_m->getByUsername($this->session->userdata('username'));
		$query= $query->result();
		// var_dump($query[0]->nik);

		
		//query
		$data['query'] = $this->Masyarakat_m->update($ava,$query[0]->nik);
		//flashdata


		 redirect(base_url('User/ProfileController'), 'refresh');
	}
}
