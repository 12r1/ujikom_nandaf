<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProfileController extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		is_logged_in();
	}

	// List all your items
	public function index()
	{
		$data['title'] = 'Profile';

		$masyarakat = $this->db->get_where('masyarakat', ['username' => $this->session->userdata('username')])->row_array();
		$petugas = $this->db->get_where('petugas', ['username' => $this->session->userdata('username')])->row_array();


		if ($masyarakat == TRUE) :
			$data['user'] = $masyarakat;
		elseif ($petugas == TRUE) :
			$data['user'] = $petugas;
		endif;

		$this->load->view('_part2/backend_head', $data);
		$this->load->view('_part2/backend_sidebar_v');
		$this->load->view('_part2/backend_topbar_v');
		$this->load->view('user/profile');
		$this->load->view('_part2/backend_footer_v');
		$this->load->view('_part2/backend_foot');
	}

	public function upload_avatar()
	{
		$this->load->model('masyarakat_m');
		$data['current_user'] = $this->auth_model->current_user();

		if ($this->input->method() === 'post') {
			// the user id contain dot, so we must remove it
			$file_name = str_replace('.', '', $data['current_user']->id);
			$config['upload_path']          = FCPATH . '/upload/avatar/';
			$config['allowed_types']        = 'gif|jpg|jpeg|png';
			$config['file_name']            = $file_name;
			$config['overwrite']            = true;
			$config['max_size']             = 1024; // 1MB
			$config['max_width']            = 1080;
			$config['max_height']           = 1080;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('avatar')) {
				$data['error'] = $this->upload->display_errors();
			} else {
				$uploaded_data = $this->upload->data();

				$new_data = [
					'id' => $data['current_user']->id,
					'avatar' => $uploaded_data['file_name'],
				];

				if ($this->profile_model->update($new_data)) {
					$this->session->set_flashdata('message', 'Avatar updated!');
					redirect(site_url('User/ProfileController'));
				}
			}
		}

		$this->load->view('user/upload_avatar', $data);
	}
}

/* End of file ProfileController.php */
/* Location: ./application/controllers/User/ProfileController.php */
