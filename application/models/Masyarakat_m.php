<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Masyarakat_m extends CI_Model
{

	private $table = 'masyarakat';
	private $primary_key = 'nik';

	public function create($data)
	{
		return $this->db->insert($this->table, $data);;
	}
	public function update($data,$nik)
	{
		return $this->db->update($this->table, $data, ['nik' => $nik]);
	}
	public function getByUsername($username)
	{
		return $this->db->get_where('masyarakat', array('username' => $username));
	}
}

/* End of file Masyarakat_m.php */
/* Location: ./application/models/Masyarakat_m.php */